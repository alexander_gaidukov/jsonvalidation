//
//  FailableDecodableArray.swift
//  JSONValidation
//
//  Created by Alexandr Gaidukov on 04.04.2022.
//

import Foundation

struct FailableDecodableArray<Value: Decodable>: Decodable, RandomAccessCollection, MutableCollection {

    var wrappedValue: [Value]

    init(from decoder: Decoder) throws {
        var container = try decoder.unkeyedContainer()
        var elements: [Value] = []
        while !container.isAtEnd {
            if let element = try? container.decode(Value.self) {
                elements.append(element)
            }
        }
        wrappedValue = elements
    }
    
    var startIndex: Int { wrappedValue.startIndex }
    var endIndex: Int { wrappedValue.endIndex }
    
    subscript(position: Int) -> Value {
        get {
            wrappedValue[position]
        }
        
        set {
            wrappedValue[position] = newValue
        }
    }
}
