//
//  Product.swift
//  JSONValidation
//
//  Created by Alexandr Gaidukov on 04.04.2022.
//

import Foundation

struct Image: Decodable {
    var id: String
    var image: String
    var thumbnailImage: String
}

struct Product: Decodable {
    let id: Int
    let title: String
    let price: Convertible<Double>  // can accept both "price": "12.5" and "price": 12.5
    let image: FailableDecodable<Image>? // will set image to nil if object is invalid
}
