//
//  ProductList.swift
//  JSONValidation
//
//  Created by Alexandr Gaidukov on 04.04.2022.
//

import Foundation

struct ProductList: Decodable {
    let products: FailableDecodableArray<Product> // will filter invalid objects
}
