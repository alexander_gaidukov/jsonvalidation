//
//  FailableDecodable.swift
//  JSONValidation
//
//  Created by Alexandr Gaidukov on 04.04.2022.
//

import Foundation

/// `@propertyWrapper` on the first glance looks better but it doesn't allow to create an Optional property
/// `@FailableDecodable var item: Int?` will be transformed to `var item: FailableDecodable<Int?>
///  It means that if `item` key is not presented in the json the parsing will failed. So we can use this approach for optional properties
/// `@dynamicMemberLookup` solves this problem

@dynamicMemberLookup
struct FailableDecodable<Value: Decodable>: Decodable {

    var wrappedValue: Value?

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        wrappedValue = try? container.decode(Value.self)
    }
    
    subscript<Prop>(dynamicMember kp: KeyPath<Value, Prop>) -> Prop? {
        wrappedValue?[keyPath: kp]
    }
    
    subscript<Prop>(dynamicMember kp: WritableKeyPath<Value, Prop>) -> Prop? {
        get {
            wrappedValue?[keyPath: kp]
        }
        
        set {
            if let value = newValue {
                wrappedValue?[keyPath: kp] = value
            }
        }
    }
}
