//
//  Convertible.swift
//  JSONValidation
//
//  Created by Alexandr Gaidukov on 04.04.2022.
//

import Foundation

/// `@propertyWrapper` on the first glance looks better but it doesn't allow to create an Optional property
/// `@FailableDecodable var item: Int?` will be transformed to `var item: Convertible<Int?>
///  It means that if `item` key is not presented in the json the parsing will failed. So we can use this approach for optional properties
/// `@dynamicMemberLookup` solves this problem

@dynamicMemberLookup
struct Convertible<Value: Decodable & LosslessStringConvertible>: Decodable {
    var wrappedValue: Value
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        guard let stringValue = try? container.decode(String.self),
            let value = Value(stringValue) else {
                wrappedValue = try container.decode(Value.self)
                return
        }
        wrappedValue = value
    }
    
    subscript<Prop>(dynamicMember kp: KeyPath<Value, Prop>) -> Prop {
        wrappedValue[keyPath: kp]
    }
    
    subscript<Prop>(dynamicMember kp: WritableKeyPath<Value, Prop>) -> Prop {
        get {
            wrappedValue[keyPath: kp]
        }
        
        set {
            wrappedValue[keyPath: kp] = newValue
        }
    }
}
